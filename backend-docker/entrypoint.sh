#!/bin/sh
if [ ! -f db.json ] ; then
	touch db.json
fi
if [ ! -s db.json ] ; then
	echo '{"projects": [ {"id": 1, "data": {"assets": [], "styles": [], "pages": []} } ]}' > db.json
fi

"$@"
